module.exports = function(express, app, fs, os, formidable, gm, knoxClient, mongoose, io) {

  var Socket;
  
  io.on('connection', function(socket){
    Socket = socket;
  });
  
  
  
  //models
  var singleImage = new mongoose.Schema({
    filename: String,
    votes:Number
  });
  
  var singleImageModel = mongoose.model('singleImage', singleImage);
  
  
  //routes
  var router = express.Router();
  
  router.get('/', function(req, res, next) {
    res.render('index', {host: app.get('host')});
  });
  
  router.post('/upload', function(req, res, next) {
  
    function generateFileName(filename) {
      var ext_regex = /(?:\.([^.]+))?$/;
      var ext = ext_regex.exec(filename)[1];
      var date = new Date().getTime();
      var charBank = "abcefghijklmnopqrstuvwxyz";
      var fString = '';
      for (var i = 0; i < 15; i++) {
        fString += charBank[parseInt(Math.random() * 26)];
  
      }
      return (fString += date + '.' + ext);
  
    }
    
    var tmpFile, nFile, fName;
  
    var newForm = new formidable.IncomingForm();
    newForm.keepExtensions = true;
    newForm.parse(req, function(err, fields, files) {
      if (err) {console.log(err);}
      tmpFile = files.upload.path;

      fName = generateFileName(files.upload.name);
      nFile = os.tmpdir() +'/' + fName;
      res.writeHead(200, {'COntent-type': 'text/plain'});
      res.end();
    });
    
    newForm.on('end', function(){
      
      fs.rename(tmpFile, nFile, function(){
        gm(nFile).resize(300).write(nFile, function() {
        //upload to s3
        fs.readFile(nFile, function(err, buf) {
          if (err) {
            return console.log("Read File Error: " + err);
          }
          var req = knoxClient.put(fName, {
            'Content-Length': buf.length,
            'Content-Type': 'image/jpeg'
          });
  
          req.on('response', function(res) {
            if (res.statusCode == 200) {
              var newImage = new singleImageModel({
                filename:fName,
                votes:0
              }).save();
              
              Socket.emit('status',  {'msg': 'Saved~', 'delay':3000});
              Socket.emit('doUpdate',{});
              
              //delete local file
              fs.unlink(nFile, function(){
                console.log('local file deleted');
              });
            }
          });
          req.end(buf);
        });
      });
      })
      
    });
  });

  router.get('/getimages', function(req, res, next) {
    singleImageModel.find({}, null, {sort:{votes:-1}}, function(err, result){
      if(err){
        console.log('Single Model Find Error: ' + err);
      }
        res.send(JSON.stringify(result));
    });
  });

  router.put('/voteup/:id', function(req, res, next) {
     singleImageModel.findByIdAndUpdate(req.params.id, {$inc:{votes:1}}, {new: true},  function(err, result){
       if(err){
         console.log("Vote Up Error: " + err);
       }
       res.send(200, {votes:result.votes});
     });
  });
  
  app.use('/', router);

};