var express = require('express'),
    path = require('path'),
    config = require('./config/config.js'),
    knox = require('knox'),
    fs = require('fs'),
    os = require('os'),
    formidable = require('formidable'),
    gm = require('gm'),
    mongoose = require('mongoose').connect(config.dbURL);


var app = express();


//establishes source for our views
app.set('views', path.join(__dirname, 'views'));

//template engine
app.engine('html', require('hogan-express'));
app.set('view engine', 'html');

//static files
app.use(express.static(path.join(__dirname, 'public')));

app.set('port', process.env.PORT || 3000);
app.set('host', config.host);


var knoxClient = knox.createClient({
    key: config.s3AccessKey,
    secret: config.s3SecertKey,
    bucket: config.S3Bucket
});

//server
var server = require('http').createServer(app);
var io = require('socket.io')(server);

require('./router/routes.js')(express, app, fs, os, formidable, gm, knoxClient, mongoose, io);

server.listen(app.get('port'), function(){
    console.log("photogrid running on port: " + app.get('port'));
});